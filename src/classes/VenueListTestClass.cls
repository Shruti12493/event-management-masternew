@isTest
public class VenueListTestClass {
    @isTest
    public static void searchVenue(){
        List<Venue__c> venList = new List<Venue__c>();
        List<Venue__c> newList = new  List<Venue__c>();
        for(Integer i=0;  i < 10; i++){
            Venue__c ven = new Venue__c();
            if(i/2==0){
                ven.name = 'oldEvent'+i;
            }
            else{
                ven.name = 'newEvent'+i;
            }
            venList.add(ven);
        }
        insert venList;
        test.StartTest();
        newList= venuelistapex.findVenueByName('oldEvent');
        test.stopTest();
        System.assertEquals(2, newList.size());
        
    }
    @isTest
    public static void getSubvenue(){
        List<Venue__c> venList = new List<Venue__c>();
        for(Integer i=0;  i < 10; i++){
            Venue__c ven = new Venue__c();
            if(i/2==0){
                ven.name = 'oldEvent'+i;
            }
            else{
                ven.name = 'newEvent'+i;
            }
            venList.add(ven);
        }
        insert venList;
        
        List<Venue__c> newList = new List<Venue__c> ();
        Venue__c venueObj = [SELECT Id,Name
                             FROM Venue__c
                             WHERE Name = 'oldEvent0'
                             LIMIT 1];
        list<Venue__c> updateList = new list<Venue__c>();
        for( Venue__c ven : [SELECT Id,Name,ParentVenue__c
                             FROM Venue__c
                             WHERE Name != 'oldEvent0']){
                                 
                                 ven.ParentVenue__c = venueObj.Id;
                                 updateList.add(ven);
                             }
        update updateList;
        List<String> objId = new List<String>();
        objId.add(venueObj.Id);
        test.startTest();
        newList = venuelistapex.getsubVenueList(objId);
        test.stoptest();
        System.assertEquals(9,newList.size()); 
    }
    @isTest
    public static void getSelectedvenuesForSecondTime(){
        List<Venue__c> newList = new List<Venue__c> ();
        Campaign campaign = new Campaign();
        campaign.name = 'vashi';
        insert campaign;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl= new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Round';
        venl.Venue__c=venue.id;
        venl.No_of_Tables__c=10;
        venl.No_of_Seats__c=10;
        venl.Layout_Row_Count__c=10;
        insert venl; 
        
        Event_Venue__c eventvenue = new Event_Venue__c();
        eventvenue.Campaign__c =campaign.id;
        eventvenue.Venue_Layout__c = venl.Id;
        eventvenue.Venue__c = venue.id;
        insert eventvenue;
        
        test.startTest();
        newList = venuelistapex.getSelectedvenues(campaign.id);
        test.stoptest();
        System.assertEquals(1,newList.size()); 
    }
    @isTest
    public static void removePreviouslySelectedVenues(){
        
        Campaign campaign = new Campaign();
        campaign.name = 'vashi';
        campaign.Start_Date__c = system.now();
        campaign.End_Date__c = system.now();
        insert campaign;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl= new Venue_Layout__c();
        venl.Name = 'abc';
        insert venl; 
        
        Event_Venue__c eventvenue = new Event_Venue__c();
        eventvenue.Campaign__c =campaign.id;
        eventvenue.Venue__c = venue.id;
        eventvenue.Venue_Layout__c = venl.Id;
        //eventvenue.Campaign__r.Start_Date__c = ;
        //eventvenue.Campaign__r.End_Date__c =  ;
        insert eventvenue;
        
        test.startTest();
        String newList = venuelistapex.deletevenues(campaign.id,venue.id);
        test.stoptest();
        
        
    }
}